\ProvidesClass{coopetition}

\LoadClass{scrartcl}

\KOMAoptions{%
 	 parskip=full,%
  	fontsize=12pt%
}%

\RequirePackage[english,ngerman]{babel}
\RequirePackage{ifluatex}
\RequirePackage{titling}
\RequirePackage[bottom=4cm,left=2cm,right=2cm,top=3cm]{geometry}
\RequirePackage{etoolbox}
\RequirePackage{graphicx}
\RequirePackage{textcomp}
\RequirePackage[pages=all,firstpage=true]{background}
\RequirePackage{iflang}
\RequirePackage{comment}
\backgroundsetup{%	
	contents={%
		\edef\@path{%
			\ifTranslation{./images/TopSecret.png}{./images/StrengGeheim.png}%
		}%
		\includegraphics[width=0.4\textwidth]{\@path}%
	},%
	scale=1.5,%
	angle=37,%
	opacity=0.15,%
%	placement=top,%
	hshift=0.2\textwidth,%
	vshift=0.1\textheight,%
}

\ifluatex
	\RequirePackage{fontspec}
\else
	\RequirePackage[utf8]{inputenc}
\fi

\RequirePackage{titlesec}

\titlespacing*{\section}{0ex}{0.5ex}{-1ex}
\titlespacing*{\subsection}{-10ex}{0.5ex}{-1ex}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% COMMANDS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\@titleImage}{}
\newcommand{\titleImage}[1]{\renewcommand{\@titleImage}{#1}}
\newcommand{\@awLogo}{}
\newcommand{\awLogo}[1]{\renewcommand{\@awLogo}{#1}}
\newcommand{\@topImage}{%
		\ifTranslation{./images/TopSecret.png}{./images/StrengGeheim.png}%
}
\newcommand{\topImage}[1]{\renewcommand{\@topImage}{#1}}

\renewcommand{\maketitle}{%
	\thispagestyle{empty}%
	\NoBgThispage
	%\vspace*{\fill}
	\begin{center}%
		\edef\@path{\@topImage}%
		\ifdefempty{\@topImage}{}{\includegraphics[width=0.3\textwidth]{\@path}}%
		
		\ifdefempty{\@titleImage}{}{%
			\edef\@path{\@titleImage}%
			\includegraphics[width=\textwidth]{\@path}%
			\vspace{1cm}
		}%
		
		\Huge{\textbf{\thetitle}}%
		
		\vspace{1cm}%
		
		\ifdefempty{\@awLogo}{{\LARGE-\,ArmAWorld\,-}}{%
			\edef\@path{\@awLogo}%
			\includegraphics[width=0.5\textwidth]{\@path}%
		}
	\end{center}%
%	
	\vspace*{\fill}%
	\pagebreak%
	\setcounter{page}{1}%
}

%%%%%%  INTERNATIONALIZATION %%%%%%%
\newcommand{\tText}[2]{%
	\ifTranslation{#2}{#1}
}
\newcommand{\ifTranslation}[2]{%
	\IfLanguageName{ngerman}{%
		#2%
	}{%
	#1%
	}%
}
\newcommand{\tTitle}[2]{%
	\title{\tText{#1}{#2}}
}
\newcommand{\tSection}[2]{%
	\section{\tText{#1}{#2}}
}
\newcommand{\tSubsection}[2]{%
	\subsection{\tText{#1}{#2}}
}
\newcommand{\tSubsubsection}[2]{%
	\subsubsection{\tText{#1}{#2}}
}
\newenvironment{original}{%
	\ifTranslation{\begin{comment}}{}
}{%
	\ifTranslation{\end{comment}}{}
}
\newenvironment{translation}{%
	\ifTranslation{}{\begin{comment}}
}{%
	\ifTranslation{}{\end{comment}}
}