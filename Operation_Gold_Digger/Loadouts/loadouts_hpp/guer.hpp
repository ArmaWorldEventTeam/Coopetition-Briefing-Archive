// miliz Basis ------------------------------------------------------------------------------------------------------
class syndicat_baseclass : BaseCollection {
    uniform[] = {"U_BG_Guerrilla_6_1","U_BG_Guerilla1_1","U_BG_Guerilla2_2","U_BG_Guerilla2_1","U_I_G_Story_Protagonist_F","U_B_ION_Uniform_01_poloshirt_wdl_F","U_B_ION_Uniform_01_tshirt_black_F"};
    vest[] = {"V_I_G_resistanceLeader_F","V_TacVest_grn"};
    backpack[] = {"B_AssaultPack_rgr"};
    headgear[] = {"H_I_Helmet_canvas_Green", "H_O_Helmet_canvas_ghex_F"};

    primaryWeapon[] = {"SMG_03C_TR_khaki"};
    primaryWeaponOptic[] = {"optic_ACO_grn_AK_F"};
    primaryWeaponLoadedMagazine[] = {"50Rnd_570x28_SMG_03"};

    handgun[] = {"hgun_ACPC2_F"};
    handgunLoadedMagazine[] = {"9Rnd_45ACP_Mag"};

    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2}};
    linkedItems[] = {"ItemMap","ItemCompass","ItemWatch","ItemSmartPhone"};
    items[] = {{"ACE_fieldDressing",4},{"ACE_splint",2},{"ACE_EarPlugs",1},{"ACRE_PRC148",1},{"ACE_morphine",2}};
};

// zugführer
class I_Soldier_SL_F : syndicat_baseclass {
    binocular[] = {"Binocular"};
    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"50Rnd_570x28_SMG_03",9}};
    items[] = {{"ACE_fieldDressing",4},{"ACE_splint",2},{"ACE_EarPlugs",1},{"ACRE_PRC148",2},{"ACE_morphine",2},{"ACE_MapTools",1}};
};

// ftl
class I_Soldier_TL_F : syndicat_baseclass {
    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"50Rnd_570x28_SMG_03",9}};
    items[] = {{"ACE_fieldDressing",4},{"ACE_splint",2},{"ACE_EarPlugs",1},{"ACRE_PRC148",2},{"ACE_morphine",2},{"ACE_MapTools",1}};
};

// lmg
class I_Soldier_AR_F : syndicat_baseclass {
    primaryWeapon[] = {"LMG_Zafir_black_F"};
    primaryWeaponLoadedMagazine[] = {"150Rnd_762x54_Box_Tracer"};
    backpack[] = {"B_Kitbag_wdl_F"};
    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"150Rnd_762x54_Box_Tracer",5}};
};

// Grenadier
class I_Soldier_GL_F : syndicat_baseclass {
    primaryWeapon[] = {"arifle_TRG21_GL_F"};
    primaryWeaponOptic[] = {"optic_ACO_grn_AK_F"};
    primaryWeaponLoadedMagazine[] = {"30Rnd_556x45_Stanag"};

    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"30Rnd_556x45_Stanag",10},{"1Rnd_HE_Grenade_shell",5}};
};

// Rifleman
class I_soldier_F : syndicat_baseclass {
    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",4},{"50Rnd_570x28_SMG_03",9}};
};

// marksman
class I_Soldier_M_F : syndicat_baseclass {
    primaryWeapon[] = {"srifle_DMR_06_hunter_F"};
    primaryWeaponOptic[] = {"optic_MRCO"};
    primaryWeaponLoadedMagazine[] = {"10Rnd_Mk14_762x51_Mag"};

    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",4},{"10Rnd_Mk14_762x51_Mag",15}};
};

// hmg schütze
class I_support_MG_F : syndicat_baseclass {
    backpack[] = {"B_Carryall_wdl_F"};

    secondaryWeapon[] = {"ace_csw_staticM2ShieldCarry"};

    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"50Rnd_570x28_SMG_03",4},{"ace_csw_100Rnd_127x99_mag_yellow",3}};
    items[] = {{"ACE_fieldDressing",4},{"ACE_splint",2},{"ACE_EarPlugs",1},{"ACRE_PRC148",2},{"ACE_morphine",2}};
};

// hmg assi
class I_support_AMG_F : syndicat_baseclass {
    backpack[] = {"B_Carryall_wdl_F"};

    secondaryWeapon[] = {"ace_csw_m3CarryTripodLow"};

    magazines[] = {{"MiniGrenade",2},{"SmokeShell",2},{"9Rnd_45ACP_Mag",2},{"50Rnd_570x28_SMG_03",6},{"ace_csw_100Rnd_127x99_mag_yellow",3}};
    items[] = {{"ACE_fieldDressing",4},{"ACE_splint",2},{"ACE_EarPlugs",1},{"ACRE_PRC148",2},{"ACE_morphine",2}};
};
